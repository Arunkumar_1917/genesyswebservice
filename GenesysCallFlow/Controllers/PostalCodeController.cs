using GenesysCallFlow.Models;
using GenesysCallFlow.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GenesysCallFlow.Controllers
{
  public class PostalCodeController : ApiController
  {
    //[Authorize]
    public IHttpActionResult GetAllPostalCodes()
    {
      using(var context = new GenesysWebServicesContext())
      {
        PostalCodeVM result = new PostalCodeVM();
        result.PostalCodeDetails = context.PostalCode.Select(s => new PostalCodeInfo()
        {
          PostalCode = s.PostalCode1,
          AgentName = s.AgentName,
          AgentEmailAddress = s.AgentEmailAddress
        }).ToArray();
        if (result.PostalCodeDetails.Length == 0)
          result.Status = "Not Found";
        else
          result.Status = "Success";
        return Ok(result);
      }
    }

    //[Authorize]
    public IHttpActionResult DetailsByPostalCode(SearchCode postalCode)
    {
      using(var context = new GenesysWebServicesContext())
      {
        PostalCodeVM result = new PostalCodeVM();
        result.PostalCodeDetails = context.PostalCode.Where(s => s.PostalCode1 == postalCode.PostalCode).Select(s => new PostalCodeInfo {
          PostalCode = s.PostalCode1,
          AgentName = s.AgentName,
          AgentEmailAddress = s.AgentEmailAddress
        }).ToArray();

        if (result.PostalCodeDetails.Length == 0)
          result.Status = "Not Found";
        else
          result.Status = "Success";

        return Ok(result);
      }
    }
  }
}
