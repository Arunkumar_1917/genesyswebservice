using GenesysCallFlow.Models;
using GenesysCallFlow.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace GenesysCallFlow.Controllers
{
  public class AgentDetailsController : ApiController
  {
    //[Authorize]
    // GET api/values
    public IHttpActionResult GetAllAgents()
    {
      var Identity = (ClaimsIdentity)User.Identity;
      //List<AgentDetailsVM> agentDetails = new List<AgentDetailsVM>() {
      //  new AgentDetailsVM() { AgentEmailAddress="arunkumar.m@smiths.com",AgentName="Arun"},
      //  new AgentDetailsVM(){AgentName="Kumar",AgentEmailAddress="arun.kumar@epicor.com"}
      //};
      //return Ok(agentDetails);
      //List<AgentDetailsVM> agentDetails = new List<AgentDetailsVM>();
      using (var dbContext = new GenesysWebServicesContext())
      {
        AgentDetailsVM result = new AgentDetailsVM();
        result.AgentDetails = dbContext.AgentDetails.Select(s => new AgentInfo()
        {
          AgentEmailAddress = s.AgentEmailAddress
        }).ToArray();
        
        if (result.AgentDetails.Length == 0)
          result.Status = "Not Found";
        else
          result.Status = "Success";

        return Ok(result);
      }
    }

    //[Authorize]
    // GET api/values/5
    public IHttpActionResult AgentDetailsByName(SearchAgent name)
    {
      using (var dbContext = new GenesysWebServicesContext())
      {
        AgentDetailsVM result = new AgentDetailsVM();
        result.AgentDetails = dbContext.AgentDetails.Where(a => a.AgentName.Contains(name.AgentName)).Select(s => new AgentInfo()
        {
        AgentEmailAddress = s.AgentEmailAddress
        }).ToArray();
    
        if (result.AgentDetails.Length == 0)
          result.Status = "Not Found";
        else
          result.Status = "Success";

        return Ok(result);
      }
    }
  }
}
