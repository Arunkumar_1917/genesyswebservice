using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenesysCallFlow.Models
{
  public class UserRepository
  {
    GenesysWebServicesContext context = new GenesysWebServicesContext();
    //This method is used to check and validate the user credentials
    public Users ValidateUser(string username, string password)
    {
      return context.Users.FirstOrDefault(user =>
      user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
      && user.Password == password);
    }
    public void Dispose()
    {
      context.Dispose();
    }
  }
}
