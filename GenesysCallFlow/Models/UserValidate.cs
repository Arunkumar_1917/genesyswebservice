using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenesysCallFlow.Models
{
  public class UserValidate
  {
    public static bool Login(string username, string password)
    {
      using(var dbContext = new GenesysWebServicesContext())
      {
        //Basic Auth Code
        return dbContext.Users.Any(user =>
        user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
        && user.Password == password);
      }
    }
  }
}
