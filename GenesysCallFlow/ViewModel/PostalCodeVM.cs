using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenesysCallFlow.ViewModel
{
  public class PostalCodeVM
  {
    public PostalCodeInfo[] PostalCodeDetails { get; set; }
    public string Status { get; set; }
  }

  public class PostalCodeInfo
  {
    public string PostalCode { get; set; }
    public string AgentName { get; set; }
    public string AgentEmailAddress { get; set; }
  }

  public class SearchCode
  {
    public string PostalCode { get; set; }
  }
}
