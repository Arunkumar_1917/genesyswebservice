using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GenesysCallFlow.ViewModel
{
  public class AgentDetailsVM
  {
    public AgentInfo[] AgentDetails { get; set; }
    public string Status { get; set; }
  }

  public class AgentInfo
  {
    public string AgentEmailAddress { get; set; }
  }

  public class SearchAgent
  {
    public string AgentName { get; set; }
  }
}
